#ifndef SOCIO_HPP
#define SOCIO_HPP

#include"cliente.hpp" //Include Necessário

#include<string> //Biblioteca Necessária
#include <iostream> //Biblioteca Necessária
#include <vector> //Biblioteca Necessária

using namespace std;

//Classe Sócio
class Socio:public Cliente{
    private: 
        string nome, telefone, email, endereco, genero;
        int tamSocio;
        vector <Socio*> socio;

    public:
        Socio(); //Construtor
        Socio(string endereco, string genero, string nome, string telefone, string email, string cpf);
        ~Socio(); //Destrutor

        //Métodos Getters e Setters:
        string get_endereco(); //get endereco
        void set_endereco(string endereco); //set endereco

        string get_email(); //get email
        void set_email(string email); //set email

        string get_telefone(); //get telefone
        void set_telefone(string telefone); //set telefone

        string get_nome(); //get nome
        void set_nome(string nome); //set nome

        string get_genero(); //get genero
        void set_genero(string genero); //set genero

        void set_tamanho_socio(); //set tamanho sócio

        void set_socio(); //set sócio


//Outros Métodos:
        void adicionaSocio();

        void arquivoSocio();

        bool confereSocio();
};

#endif
