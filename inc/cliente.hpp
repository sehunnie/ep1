#ifndef CLIENTE_HPP
#define CLIENTE_HPP

#include <iostream> //Biblioteca Necessária
#include <string>  //Biblioteca Necessária

using namespace std;

//Classe Cliente
class Cliente{
    private:
        string cpf;
    
    public: 
        Cliente(); //Construtor
        Cliente (string cpf);
        ~Cliente(); //Destrutor

        //Métodos Getters e Setters:
        string get_cpf(); //get cpf
        void set_cpf(string cpf); //set cpf
};

#endif