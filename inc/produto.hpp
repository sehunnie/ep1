#ifndef PRODUTO_HPP
#define PRODUTO_HPP

#include <iostream> //Biblioteca Necessária
#include <string>  //Biblioteca Necessária
#include <fstream> //Biblioteca Necessária
#include <vector> //Biblioteca Necessária

using namespace std;

//Classe Produto
class Produto{
    private:
        string produto, marca;
        int qtde, tamTipo;
        float preco;
        vector <string> vectorTipo;

    public: 
        Produto(); //Construtor
        Produto(string produto, string marca, vector <string> vectorTipo, float preco, int qtde);
        ~Produto(); //Destrutor

        //Métodos Getters e Setters:
        string get_produto(); //get produto
        void set_produto(string produto); //set produto

        string get_marca(); //get marca
        void set_marca(string marca); //set marca

        int get_qtde(); //get quantidade
        void set_qtde(int qtde); //set quantidade

        int get_tamTipo(); //get tamanho Tipo
        void set_tamTipo(); //set tamanho Tipo

        float get_preco(); //get preço
        void set_preco(float preco); //set preço

        vector <string> get_vectorTipo(); //Vector Tipo

        string get_tipo(int i); //Get tipo
        void set_tipo(vector <string> tipo); //Set Tipo




};

#endif
