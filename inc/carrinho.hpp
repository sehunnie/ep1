#ifndef CARRINHO_HPP
#define CARRINHO_HPP

#include "produto.hpp" //Include Necessário
#include "estoque.hpp" //Include Necessário

#include <iostream> //Biblioteca Necessária
#include <string>  //Biblioteca Necessária

using namespace std;

//Classe Carrinho
class Carrinho:public Estoque{
    private: 
        vector <Produto*> listaCompras;
        int tamCarrinho; 
        float precoFinal;
        
    public:   
        Carrinho(); //Construtor
        Carrinho(int qtde);
        ~Carrinho(); //Destrutor

        //Métodos Getters e Setters:
        int get_tamCarrinho(); //get tamanho carrinho
        void set_tamCarrinho(); //set tamanho carrinho

        float get_precoFinal(); //get preço final
        void set_precoFinal(); //set preço final

        vector<Produto*> get_listaCompras();

        //Outros Métodos:

        bool realizaPagamento(); //Adiciona Produto
        
        void retProduto(); // Retira produto       

        void cadastraProduto(); //Cadastra Produto
};

#endif
