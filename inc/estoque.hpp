#ifndef ESTOQUE_HPP
#define ESTOQUE_HPP

#include "produto.hpp" //Include Necessário

#include <iostream> //Biblioteca Necessária
#include <vector> //Biblioteca Necessária
#include <string> //Biblioteca Necessária
#include <stdbool.h> //Biblioteca Necessária

//Classe Estoque
class Estoque{
    private:
        vector<string> tipo;
        vector<Produto*> estoqueProduto;

        int tamTipo;
        int tamEstoque;

  public:
        Estoque(); //Construtor
        ~Estoque(); // Destrutor

//Métodos Getters e Setters:
    void set_estoque(); //Set Estoque

    void get_tipo(); //Get tipo
    void set_tipo(); //Set tipo

    int get_tamTipo(); //Get Tipo
    void set_tamTipo(); //Set Tipo

    int get_tamEstoque(); //Get Tamanho Estoque
    void set_tamEstoque(); //Set Tamanho Estoque

    void get_estoqueProduto(); //Get Estoque Produto

    Produto* get_produtoEstoque(string produto); 

//Outros Métodos:
    bool confereEstoque(); //Confere o Estoque

    bool confereTipo(); //Confere o Tipo

    void carregaTipo(); //Carrega o Tipo

    void mudaQtde(); //Muda Quantidade
    void mudaQtde(string produto, int qtde); //Muda Quantidade

    virtual void cadastraProduto(); //Cadastra Produto

    void arquivoEstoque(); //Arquivo Estoque

    void arquivoTipo(); //Arquivo Tipo
};

#endif
