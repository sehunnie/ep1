#include "carrinho.hpp" //Include Necessário
#include "estoque.hpp" //Include Necessário
#include "produto.hpp" //Include Necessário

#include <iostream> //Biblioteca Necessária
#include <string>  //Biblioteca Necessária

//Construtor:
//------------PARTICULAR------------
Carrinho::Carrinho(){
  set_precoFinal();
  set_tamEstoque();
  set_estoque();
  set_tamCarrinho();
}

//Acessores do tamanho da lista de compras
void Carrinho::set_tamCarrinho(){
  this->tamCarrinho = this->listaCompras.size();
}

int Carrinho::get_tamCarrinho(){
  return tamCarrinho;
}

//Método que adquire o nome dos produtos existentes no carrinho
vector <Produto*> Carrinho::get_listaCompras(){
    for(int i=0; i<this->tamCarrinho; i++){
      std::cout << "Nome: "<< this->listaCompras[i]->get_produto() << endl;
      std::cout << "Quantidade: "<< this->listaCompras[i]->get_qtde() << endl;

      for(int j=0; j<this->listaCompras[i]->get_tamTipo(); j++){
        std::cout << "Categoria: " << this->listaCompras[i]->get_tipo(j) << endl;
      }
    }
    return listaCompras;
}

//Acessores dos métodos da soma dos produtos do carrinho
void Carrinho::set_precoFinal(){
  if(this->tamCarrinho!=0){
    this->precoFinal=0;

    for(int i=0; i<this->tamCarrinho; i++){
      this->precoFinal+=this->listaCompras[i]->get_preco()*this->listaCompras[i]->get_qtde();
      }
    }
    else{
      this->precoFinal=0;
    }
}
float Carrinho::get_precoFinal(){
  return this->precoFinal;
}

//Confere se o produto existe. Se existir, adiciona ao carrinho
void Carrinho::cadastraProduto(){
  string nomeProduto;
  std::cout << "Produto: " << endl;
  cin.ignore();
  getline(cin, nomeProduto);
  for(int i=0; i<this->tamCarrinho; i++){
    if(nomeProduto==this->listaCompras[i]->get_produto()){
      this->listaCompras[i]->set_qtde(this->listaCompras[i]->get_qtde()+1);
      return;
    }
  }
  Produto *newProduto;
  newProduto = get_produtoEstoque(nomeProduto);

  if(newProduto==NULL){
    return;
  }
  newProduto->set_qtde(1);
  if(newProduto!=NULL){
    this->listaCompras.push_back(newProduto);
    this->tamCarrinho++;
    cout << "*** Produto adicionado ao carrinho ***" << endl;
  }
}

//Método que retira produto do carrinho
void Carrinho::retProduto(){
  string nomeProduto;
  std::cout << "Produto a ser retirado: " << endl;
  cin.ignore();
  getline(cin, nomeProduto);
  for(int i=0;i<this->tamCarrinho; i++){
    if(nomeProduto==this->listaCompras[i]->get_produto()){
      this->listaCompras.erase(this->listaCompras.begin()+i);
      this->tamCarrinho--;
      std::cout << "*** Produto removido ***" << endl;
      return;
    }
  }
  std::cout << "*** OPERAÇÃO INVÁLIDA: ***" << endl;
  std::cout << "Não há produtos no carrinho" << endl;
}

//Método que finaliza a compra
bool Carrinho::realizaPagamento(){
  int operacao;
  set_precoFinal();
  system("clear");

  std::cout << "*** Produtos comprados ***" << endl << endl;
  for(int i=0; i<this->tamCarrinho; i++){
    std::cout << listaCompras[i]->get_produto() << " - R$ " << listaCompras[i]->get_preco() << " - x" << listaCompras[i]->get_qtde() << endl;
  }
  std::cout << "Valor total da compra [Em reais]: R$ " << get_precoFinal() << endl;
  std::cout << "Desconto [Em reais]: R$ " << get_precoFinal() * 0.15 << endl;
  std::cout << "Valor final da compra [Em reais]: R$ " << get_precoFinal() - (get_precoFinal() * 0.15) << endl;

  std::cout << "Aperte '1' para finalizar a compra" << endl;
  std::cin >> operacao;
  if(operacao==1){
    Produto* newProduto;

    for(int i=0; i<this->tamCarrinho; i++){
      newProduto = get_produtoEstoque(this->listaCompras[i]->get_produto());
      if(newProduto->get_qtde()<this->listaCompras[i]->get_qtde()){
        std::cout << "*** ERROR ***" << endl;
        std::cout << "Produto Indisponível" << endl;
        cin.ignore();
        getchar();
        return true;
      }
    }
    for(int i=0; i<this->tamCarrinho; i++){
      mudaQtde(listaCompras[i]->get_produto(), listaCompras[i]->get_qtde());
    }
    std::cout << "*** GRATIDÃO PELA PREFERÊNCIA ***" << endl << endl;
    std::cout << "*** aperte 'ENTER' para sair ***" << endl;
    cin.ignore();
    getchar();
    return true;
  }
  else{
    std::cout << "*** OPERAÇÃO INVÁLIDA: ***retire o(s) produto(s) do carrinho" << endl;
    std::cout << "retire produto(s) do carrinho" << endl;
    return false;
  }
}
