#include "socio.hpp" //Include Necessário
#include "cliente.hpp" //Include Necessário

#include<iostream> //Biblioteca Necessária
#include<string> //Biblioteca Necessária
#include <fstream> //Biblioteca Necessária
#include <stdbool.h> //Biblioteca Necessária

using namespace std;

//Construtor:
//------------PARTICULAR------------
Socio::Socio(){
    set_nome("");
    set_cpf("");
    set_email("");
    set_genero("");
    set_telefone("");
    set_endereco("");
    set_tamanho_socio();
    set_socio();
}

//------------GENÉRICO------------
Socio::Socio(string nome, string cpf, string email, string telefone, string genero, string endereco){
    set_nome(nome);
    set_cpf(cpf);
    set_email(email);
    set_genero(genero);
    set_telefone(telefone);
    set_endereco(endereco);
}

//Destrutor:
Socio::~Socio(){
}

//Métodos Getters e Setters:

//Get Endereço:
string Socio::get_endereco(){
    return endereco;
}
//Set Endereco:
void Socio::set_endereco(string endereco){
    this->endereco = endereco;
}

//Get Genero:
string Socio::get_genero(){
    return genero;
}
//Set Genero:
void Socio::set_genero(string genero){
    this->genero = genero;
}

//Get email:
string Socio::get_email(){
  return email;
}
//Set email: 
void Socio::set_email(string email){
  this->email = email;
}

//Get telefone:
string Socio::get_telefone(){
  return telefone;
}
//Set telefone: 
void Socio::set_telefone(string telefone){
  this->telefone = telefone;
}

//Get nome:
string Socio::get_nome(){
  return nome;
}
//Set nome: 
void Socio::set_nome(string nome){
  this->nome = nome;
}

//Outros Métodos:

//Metódo que obtém o número de Sócios:
void Socio::set_tamanho_socio(){
  ifstream socio;
  string tamSocio;
  socio.open("Socio.txt");

  if(socio.is_open()){
    getline(socio, tamSocio);
    this->tamSocio=atoi(tamSocio.c_str());
    socio.close();
  }

  else{
    this->tamSocio = 0;
  }
}

//Método que adiciona o arquivo (txt) sócio:
void Socio::set_socio(){
  if(confereSocio()==true){
    string lixo;
    string nome, cpf, email, telefone, genero, endereco;
    ifstream socio;

    socio.open("Socio.txt");
    getline(socio, lixo);
    Socio* listaSocios;
    for(int cont = 0; cont <this->tamSocio; cont++){
      getline(socio, nome);
      getline(socio, cpf);
      getline(socio, email);
      getline(socio, genero);
      getline(socio, telefone);
      getline(socio, endereco);

      listaSocios=new Socio(nome, cpf, email, telefone, genero, endereco);
      this->socio.push_back(listaSocios);
    }
    socio.close();
  }
  else if(confereSocio()==false){
    std::cout << "Não tem sócio" << endl;
  }
}

//Método que confere se existe o arquivo (txt) sócio:
bool Socio::confereSocio(){
  ifstream socio;
  socio.open("Socio.txt");
  if(socio.is_open()){
    socio.close();
    return true;
  }
  else{
    return false;
  }
}

  //Método que cadastra sócios
void Socio::adicionaSocio(){
  string nome, cpf, email, telefone, genero, endereco;

  std::cout << "Digite o seu CPF: [Exemplo: 000.000.000-00]" << endl;
  std::cin >> cpf;

  while(cpf.size()!=14){
    std::cin >> cpf;
  }
  for(int i=0; i<this->tamSocio; i++){
    if(cpf==this->socio[i]->get_cpf()){
      std::cout << "*** Sócio já cadastrado ***" << endl;
      return;
    }
  }
  std::cout << "*** Sócio não encontrado ***" << endl;
  std::cout << "*** Por favor, insira seus dados para o cadastro ***" << endl;

  std::cout << "Digite seu nome: [Exemplo: Sehun Silva]" << endl;
  cin.ignore();
  getline(cin, nome);

  std::cout << "Digite seu email: [Exemplo: sehun@exo.com] " << endl;
  std::cin >> email;

  std::cout << "Digite seu telefone: [Exemplo: 90000-0000]" << endl;
  std::cin >> telefone;

  std::cout << "Digite seu Gênero: " << endl;
  std::cout << "M - Mulher; H - Homem; N - Não Binário; I - Prefiro não indentificar " << endl;
  std::cin >> genero;

  std::cout << "Digite seu endereço: " << endl;
  std::cin >> endereco;

  Socio *listaSocios = new Socio(nome, cpf, email, telefone, genero, endereco);
  this->socio.push_back(listaSocios);

  cout << "*** Sócio cadastrado com sucesso ***" << endl;
  this->tamSocio++;
  cin.ignore();
  getchar();
    cout << "*** Aperte 'ENTER' para continuar ***" << endl;
}

//Método que adiciona no arquivo (txt) os sócios:
void Socio::arquivoSocio(){
    if(confereSocio()==true||this->socio.size()!=0){
        ofstream socio;
        socio.open("socio.txt");
        socio << this->tamSocio << endl;

        for(int k=0; k<this->tamSocio; k++){
            socio << this->socio[k]->get_nome() << endl;
            socio << this->socio[k]->get_cpf() << endl;
            socio << this->socio[k]->get_email() << endl;
            socio << this->socio[k]->get_telefone() << endl;
            socio << this->socio[k]->get_genero() << endl;
            socio << this->socio[k]->get_endereco() << endl;
        }
        socio.close();
    }
    else{

    }
}