#include "produto.hpp" //Include Necessário
#include "estoque.hpp" //Include Necessário

#include <iostream> //Biblioteca Necessária
#include <stdbool.h> //Biblioteca Necessária
#include <vector> //Biblioteca Necessária
#include <fstream> //Biblioteca Necessária
#include <string> //Biblioteca Necessária

using namespace std;

//Construtor:
//------------GENÉRICO------------
 Estoque::Estoque(){
    set_tamEstoque();
    set_estoque();
    set_tamTipo();
    carregaTipo();
}

//Destrutor:
Estoque::~Estoque(){
}

//Outros Métodos:

//Método que define tamanho do Estoque
void Estoque::set_tamEstoque(){
    ifstream estoque;
    string tamEstoque;
    estoque.open("Estoque.txt");

    if(estoque.is_open()){
        getline(estoque, tamEstoque);
        this->tamEstoque=atoi(tamEstoque.c_str());
        estoque.close();
    }
    else{
    this->tamEstoque = 0;
    }
}
int Estoque::get_tamEstoque(){
  return tamEstoque;
}

//Método para buscar um produto
Produto* Estoque::get_produtoEstoque(string produto){
    for(int i=0; i<this->tamEstoque; i++){
        if(produto==estoqueProduto[i]->get_produto()){
            Produto* newProduto = new Produto(estoqueProduto[i]->get_produto(), estoqueProduto[i]->get_marca(), estoqueProduto[i]->get_vectorTipo(), estoqueProduto[i]->get_preco(), estoqueProduto[i]->get_qtde());
            return newProduto;
        }
    }
    std::cout << "*** O Produto procurado não existe ***" << endl;
    return NULL;
}

//Método para retornar produtos 
void Estoque::get_estoqueProduto(){
    for(int i=0; i<this->tamEstoque; i++){
        std::cout << "Nome do Produto: " << this->estoqueProduto[i]->get_produto()<<endl;
        std::cout << "Marca do Produto: " << this->estoqueProduto[i]->get_marca()<<endl;
        std::cout << "Tipo: ";

        for(int cont=0; cont<this->estoqueProduto[i]->get_tamTipo(); cont++){
          std::cout << this->estoqueProduto[i]->get_tipo(cont) << "|";
        }
        std::cout << endl << "Valor: R$" << this->estoqueProduto[i]->get_preco() << endl;
        std::cout << "Quantidade: "<<this->estoqueProduto[i]->get_qtde() << endl;
    }
}

//Método que confere se existe o arquivo (txt) estoque 
bool Estoque::confereEstoque(){
    ifstream estoque;
    estoque.open("Estoque.txt");

    if(estoque.is_open()){
        estoque.close();
        return true;
    }
    else
        return false;
}

//Método que carrega o arquivo
void Estoque::set_estoque(){
    if(confereEstoque()==true){
        string nomeProduto, marca, preco, qtde, lixo, categoria, tipo;
        ifstream estoque;
        int quantidade, tamTipo;
        float fpreco;

        estoque.open("estoque.txt");
        getline(estoque, lixo);
        Produto* newProduto;

        for(int i=0; i<this->tamEstoque; i++){
            vector <string> tipos;
            getline(estoque,nomeProduto);
            getline(estoque, marca);
            getline(estoque, categoria);
            tamTipo=atoi(categoria.c_str());

            for(int j=0; j<tamTipo; j++){
                getline(estoque, tipo);
                tipos.push_back(tipo);
            }

            getline(estoque, preco);
            getline(estoque,qtde);

            quantidade=atoi(qtde.c_str());

            fpreco=atof(preco.c_str());

            newProduto = new Produto(nomeProduto, marca, tipos, fpreco, quantidade);
            newProduto->set_tamTipo();
            this->estoqueProduto.push_back(newProduto);
        }
        estoque.close();
    }
    if(confereEstoque()==false){
    }
}

//Método que cadastra os produtos
void Estoque::cadastraProduto(){
    vector <string> tipos;
    string nomeProduto, tipo, marca;
    int qtde, tamTipo, k = 0;
    float preco;

    std::cout << "Digite o nome do produto: " << endl;
    cin.ignore();
    getline(cin, nomeProduto);

    std::cout << "Digite a marca do produto: " << endl;
    cin.ignore();
    getline(cin, marca);

    for(int i=0; i<this->tamEstoque; i++){
        if(nomeProduto == this->estoqueProduto[i]->get_produto()){
            std::cout << "*** Produto já cadastrado ***" << endl;
            return;
        }
    }
    std::cout << endl << "*** Produto não cadastado ***" << endl;
    std::cout << endl << "Digite quantas categorias desse produto" << endl;
    std::cin>>tamTipo;
    cin.ignore();

    for(int i=0; i<tamTipo; i++){
        std::cout << endl << "Categoria"<< i <<": " << endl;
        getline(cin, tipo);
        tipos.push_back(tipo);
    }
    std::cout << endl << "Preço do Produto: " << endl;
    std::cin >> preco;
    std::cout << endl << "Quantidade: " << endl;
    cin.ignore();
    std::cin >> qtde;

    for(int i=0; i<this->tamTipo; i++){
        for(int j=0; j<tamTipo; j++){
            if(tipos[j]==this->tipo[i]){
                k++;
            }
        }
    }
    if(k == tamTipo){
        Produto* newProduto = new Produto(nomeProduto, marca, tipos, preco, qtde);
        newProduto->set_tamTipo();
        this->estoqueProduto.push_back(newProduto);
        this->tamEstoque++;
        std::cout << "*** Produto cadastrado ***" << endl;
        return;
    }
    std::cout << "*** Categoria não existente ***" << endl;
}

//Método que salva arquivo (txt) do Estoque
void Estoque::arquivoEstoque(){
    if(confereEstoque() == true || this->estoqueProduto.size()!=0){
        ofstream estoque;
        estoque.open("Estoque.txt");
        estoque << this->tamEstoque << endl;

    for(int i=0;i<this->tamEstoque;i++){
        estoque << this->estoqueProduto[i]->get_produto() << endl;
        estoque << this->estoqueProduto[i]->get_tamTipo() << endl;

        for(int j=0;j<this->estoqueProduto[i]->get_tamTipo();j++){
            estoque << this->estoqueProduto[i]->get_tipo(j) << endl;
        }

        estoque << this->estoqueProduto[i]->get_preco() << endl;
        estoque << this->estoqueProduto[i]->get_qtde() << endl;
        estoque << this->estoqueProduto[i]->get_marca() << endl;
    }
    estoque.close();

    }else{
    }
}

//Método que salva arquivo txt das categorias
void Estoque::arquivoTipo(){
    if(confereTipo()==true||tipo.size()!=0){
        ofstream tipos;
        tipos.open("Categorias.txt");
        tipos << this->tamTipo << endl;

        for(int i=0; i<this->tamTipo; i++){
            tipos << this->tipo[i] << endl;
        }
        tipos.close();
    }
    else{
    }
}

//Método que confere se existe o arquivo categoria txt
bool Estoque::confereTipo(){
    ifstream tipos;
    tipos.open("Categorias.txt",ios::in);

    if(tipos.is_open()){
        tipos.close();
        return true;
    }
    else{
        return false;
    }
}

//Método que carrega o arquivo txt
void Estoque::carregaTipo(){
    if(confereTipo()==true){
        string tipo, lixo;
        ifstream tipos;
        tipos.open("Categorias.txt",ios::in);
        getline(tipos, lixo);

        for(int i=0; i<this->tamTipo; i++){
            getline(tipos, tipo);
            this->tipo.push_back(tipo);
        }
        tipos.close();
    }
    if(confereTipo()==false){
    }
}

//Acessores (get e set) da categoria.
void Estoque::get_tipo(){ //Get
    for(int i=0; i<this->tamTipo; i++){
        std::cout << this->tipo[i] << endl;
    }
}

void Estoque::set_tipo(){ //Set
    string tipo;
    std::cout << "Digite o nome da categoria que deseja adicionar:" << endl;
    cin.ignore();
    getline(cin, tipo);
    this->tipo.push_back(tipo);
    std::cout << "*** Categoria adicionada ***" << endl;
    this->tamTipo++;
}

//Método que determina a quantidade de categorias
void Estoque::set_tamTipo(){
    ifstream tipos;
    string tamTipo;
    tipos.open("Categorias.txt",ios::in);

    if(tipos.is_open()){
        getline(tipos, tamTipo);
        this->tamTipo = atoi(tamTipo.c_str());
        tipos.close();
    }
    else{
        this->tamTipo = 0;
    }
}

//Método que retorna número da categoria
int Estoque::get_tamTipo(){
  return this->tamTipo;
}


//Função para alterar a quantidade de um determinado produto.
void Estoque::mudaQtde(){
    string nomeProduto;
    int qtde;

    std::cout<<"Digite o produto: " << endl;
    cin.ignore();
    getline(cin, nomeProduto);

    for(int i=0; i<this->tamEstoque; i++){
        if(nomeProduto == this->estoqueProduto[i]->get_produto()){
            std::cout << "Digite a quantidade de produtos: ";
            std::cin >> qtde;
            estoqueProduto[i]->set_qtde(qtde);
            cout << "*** Quantidade atualizada ***" << endl;
            return;
        }
    }
    std::cout << "*** Produto não encontrado ***" << endl;
}

//Método que altera quantidade
void Estoque::mudaQtde(string produto, int qtde){
    for(int i=0; i<this->tamEstoque; i++){
        if(produto==this->estoqueProduto[i]->get_produto()){
            estoqueProduto[i]->set_qtde(estoqueProduto[i]->get_qtde() - qtde);
            return;
        }
    }
}