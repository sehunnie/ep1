#include "cliente.hpp" //Include Necessário
#include "produto.hpp" //Include Necessário
#include "carrinho.hpp" //Include Necessário
#include "socio.hpp" //Include Necessário
#include "estoque.hpp" //Include Necessário

#include <iostream> //Biblioteca Necessária
#include <string> //Biblioteca Necessária
#include <vector> //Biblioteca Necessária
#include <fstream> //Biblioteca Necessária

using namespace std;

int main(){

    system("clear");
//------------VARIÁVEIS------------
    int opc, op1, op2;
    // char op1, op2;
    bool final = 0;
    string nome, cpf, email, telefone, genero, endereco;
    Produto produto;
//------------VECTOR SÓCIO------------
    Socio* listaSocios = new Socio();
//------------------------------------    
    do{
        system("clear");    
//------------TERMINAL------------
        cout << "======== BOAS VINDAS A PAPELARIA SEHUNNIE ========" << endl;
        cout << "___________________________________________________" << endl;
        cout << "Qual opção você deseja?" << endl;
        cout << "(1) Modo venda" << endl;
        cout << "(2) Modo estoque" << endl;
        cout << "(9) Sair" << endl;
        cout << "___________________________________________________" << endl;
    while(op1!=9 && op1!=1 && op1!=2){
      std::cin >> op1;
    }

    cin.ignore();
    system("clear");
//------------OPÇÕES MODO VENDA------------
    switch(op1){
        case 1:{
            listaSocios->adicionaSocio();
            listaSocios->arquivoSocio();

            getchar();
            Carrinho* newCarrinho = new Carrinho();
            std::cout << "*** Aperte enter para continuar ***" << endl;
            getchar();

            do{
                system("clear");
//------------MENU VENDA------------
                cout << "======== MODO VENDA ========" << endl;
                cout << "Qual opção você deseja?" << endl;
                cout << "(1) Mostrar Estoque" << endl;
                cout << "(2) Adicionar produto ao carrinho" << endl;
                cout << "(3) Retirar produto do carrinho" << endl;
                cout << "(4) Mostrar carrinho" << endl;
                cout << "(5) Finalizar compra" << endl;
                cout << "(0) Voltar" << endl;

            while(op2!=0 && op2!=1 &&op2!=2 &&op2!=3 && op2!=4 &&op2!=5){
                std::cin >> op2;
          }
            system("clear");
//------------CÓDIGO MODO VENDA-----------

            switch(op2){
                case 1:{
                    newCarrinho->get_estoqueProduto();
                    cout << endl << "*** Aperte enter para continuar ***" << endl;
                    getchar();
                    break;
                }

                case 2:{
                    newCarrinho->cadastraProduto();
                    cout << endl << "*** Aperte enter para continuar ***" << endl;
                    getchar();
                    break;
                }

                case 3:{
                    newCarrinho->retProduto();
                    cout << endl << "*** Aperte enter para continuar ***" << endl;
                    getchar();
                    break;
                }

                case 4:{
                    newCarrinho->get_listaCompras();
                    cin.ignore();
                    cout << endl << "*** Aperte enter para continuar ***" << endl;
                    getchar();
                    break;
                }

                case 5:{
                    final = newCarrinho->realizaPagamento();
                    system("clear");

                    if(final == false){
                        opc = 0;
                    }
                    else{
                        opc = 1;
                         //...
                    }
                break;
                }
            }
        } while(op2==1||op2==2||op2==3||op2==4||(op2!=0 && opc==0));
            newCarrinho->arquivoEstoque();
            newCarrinho->arquivoTipo();
            break;
      }
//------------MODO ESTOQUE------------
      case 2:{
        Estoque *estoqueProduto = new Estoque();
        do{
          system("clear");
//------------MENU ESTOQUE------------
            cout << "======== MODO ESTOQUE ========" << endl;
            cout << "Qual opção você deseja?" << endl;
            cout << "(1) Mostrar estoque" << endl;
            cout << "(2) Adicionar produto" << endl;
            cout << "(3) Mostrar categorias" << endl;
            cout << "(4) Adicionar categoria" << endl;
            cout << "(5) Mudar a quantidade de produtos" << endl;
            cout << "(0) Voltar" << endl;
        while(op2!=0 && op2!=1 && op2!=2 && op2!=3 && op2!=4 && op2!=5){
        std::cin >> op2;
      }
//------------CÓDIGO DO ESTOQUE------------
            switch(op2){
                case 1:{
                    estoqueProduto->get_estoqueProduto();
                    cin.ignore();
                    cout << endl << "*** Aperte enter para continuar ***" << endl;
                    getchar();
                    break;
                }

                case 2:{
                    estoqueProduto->cadastraProduto();
                    cout << endl << "*** Aperte enter para continuar ***" << endl;
                    cin.ignore();
                    getchar();
                    break;
                }

                case 3:{
                    estoqueProduto->get_tipo();
                    cin.ignore();
                    cout << endl << "*** Aperte enter para continuar ***" << endl;
                    getchar();
                    break;
                }

                case 4:{
                    estoqueProduto->set_tipo();
                    cout << endl << "*** Aperte enter para continuar ***." << endl;
                    getchar();
                    break;
                }

                case 5:{
                    estoqueProduto->mudaQtde();
                    cout << endl << "*** Aperte enter para continuar ***" << endl;
                    getchar();
                    break;
                }
            }
        } while(op2==1||op2==2||op2==3||op2==4||(op2!=0 && opc==0));
            estoqueProduto->arquivoEstoque();
            estoqueProduto->arquivoTipo();
            break;
        }
         case 9:{
            listaSocios->arquivoSocio();
        break;
        }
    }
    }while(op1!=0);

    
    return 0;
}
