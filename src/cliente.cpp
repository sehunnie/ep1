#include "cliente.hpp" //Include Necessário

#include <iostream> //Biblioteca Necessária
#include <string>  //Biblioteca Necessária

//Construtor:
//------------PARTICULAR------------
Cliente::Cliente(){
  set_cpf("");
}
//------------GENÉRICO------------
Cliente::Cliente(string cpf){
  set_cpf(cpf);
} 
//Destrutor:
Cliente::~Cliente(){
}

//Métodos Getters e Setters:

//Get cpf:
string Cliente::get_cpf(){
  return cpf;
}
//Set cpf: 
void Cliente::set_cpf(string cpf){
  this->cpf = cpf;
}





