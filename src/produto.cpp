#include "produto.hpp" //Include Necessário

#include <iostream> //Biblioteca Necessária
#include <string>  //Biblioteca Necessária
#include <fstream> //Biblioteca Necessária

//Construtor:
//------------PARTICULAR------------
Produto::Produto(){
    set_produto("");
    set_marca("");
    set_preco(0);
}

//------------GENÉRICO------------
Produto::Produto(string produto, string marca, vector <string> vectorTipo, float preco, int qtde){
    set_produto(produto);
    set_marca(marca);
    set_tipo(vectorTipo);
    set_preco(preco);
    set_qtde(qtde);
}

//Destrutor:
Produto::~Produto(){
    cout << "Destrutor do Produto" << endl << endl;
}

//Métodos Getters e Setters:

//Get Produto:
string Produto::get_produto(){
  return produto;
}
//Set Produto: 
void Produto::set_produto(string produto){
  this->produto = produto;
}

//Get Marca:
string Produto::get_marca(){
  return marca;
}
//Set Marca: 
void Produto::set_marca(string marca){
  this->marca = marca;
}

//Get Tipo:
string Produto::get_tipo(int i){
  return vectorTipo[i];
}

//Set Tipo:
void Produto::set_tipo(vector<string> vectorTipo){
  this->vectorTipo = vectorTipo;
}

vector <string> Produto::get_vectorTipo(){
    return this->vectorTipo;
}

//Get Quantidade:
int Produto::get_qtde(){
  return qtde;
}
//Set Quantidade: 
void Produto::set_qtde(int qtde){
  this->qtde = qtde;
}

//Get Preco:
float Produto::get_preco(){
  return preco;
}
//Set Preco: 
void Produto::set_preco(float preco){
  this->preco = preco;
}

int Produto::get_tamTipo(){ //Get Tamanho Tipo
  return tamTipo;
}
void Produto::set_tamTipo(){
  this->tamTipo = vectorTipo.size();
}



